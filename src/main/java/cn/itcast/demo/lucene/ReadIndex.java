package cn.itcast.demo.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import java.io.File;

/**
 * @author gaoxiaoqiang
 * @date 2019-06-28 17:44
 */
public class ReadIndex {

    @Test
    public void read() throws Exception {


        FSDirectory open1 = FSDirectory.open(new File("D:\\pinyougou89\\common\\day01\\index89"));
        DirectoryReader open = DirectoryReader.open(open1);
        IndexSearcher indexSearcher = new IndexSearcher(open);
        BooleanQuery query = new BooleanQuery();

        //这是在通过改变查询条件改变查询结果
        TermQuery termQuery1 = new TermQuery(new Term("fileName", "传智播客"));
        TermQuery termQuery2 = new TermQuery(new Term("fileName", "阴阳师"));
        //
        //        //根据匹配条件查询
        query.add(termQuery1, BooleanClause.Occur.MUST);
        query.add(termQuery2, BooleanClause.Occur.SHOULD);
        TopDocs docs = indexSearcher.search(query, 10);
        int totalHits = docs.totalHits;
        System.out.println("匹配上的总文档数" + totalHits);
        //文档的集合
        ScoreDoc[] scoreDocs = docs.scoreDocs;
        for (ScoreDoc scoreDoc : scoreDocs) {
            int doc = scoreDoc.doc;
            //脱裤子放屁
            Document doc1 = indexSearcher.doc(doc);
            System.out.println(doc1.get("fileName"));
            System.out.println(doc1.get("fileContent"));
            System.out.println(doc1.get("fileSize"));
            System.out.println(doc1.get("filePath"));

        }

    }
}