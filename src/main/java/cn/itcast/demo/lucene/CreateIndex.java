package cn.itcast.demo.lucene;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.io.Reader;

/**
 * @author gaoxiaoqiang
 * @date 2019-06-28 15:21
 */
public class CreateIndex {

    @Test
    public void createIndex() throws Exception {
        System.out.println("1111111111111111111111111111");
        System.out.println("22222222222222222222222222222");
        System.out.println("3333333333333333333333333333");

        //数据产生变化用IndexWriter
        //准备索引库目录位置
        // 从下往上   缺啥补啥     //target
        FSDirectory directory = FSDirectory.open(new File("D:\\pinyougou89\\common\\day01\\index89"));
        Analyzer analyzer = new IKAnalyzer();
        //准备索引库数据源  用indexWriter写
        //D:\pinyougou89\common\day01\from
        IndexWriterConfig conf = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, conf);
        //由于文件夹中不只一个文件  先获取一个数组   //from
        File file = new File("D:\\pinyougou89\\common\\day01\\from");
        File[] files = file.listFiles();
        for (File file1 : files) {
            //创建文档对象
            Document document = new Document();

            //给文档对象赋值
            //  参数名称  字段内容  参数是否存储
            //字段名
            document.add(new TextField("fileName", file1.getName(), Field.Store.YES));
            document.add(new TextField("fileContent", FileUtils.readFileToString(file1), Field.Store.YES));
            document.add(new LongField("fileSize", FileUtils.sizeOf(file1), Field.Store.YES));
            document.add(new StringField("filePath", file1.getPath(), Field.Store.YES));
            //把文档的信息写入索引库  一个文档相当于数据库中一行
            indexWriter.addDocument(document);
        }
        indexWriter.commit();
        indexWriter.close();
    }

}

